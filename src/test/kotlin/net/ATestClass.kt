package net

import org.junit.jupiter.api.Test
import org.springframework.core.io.ClassPathResource

internal class ATestClass {

    @Test
    fun `mapping of event json payload to eventstore model object`() {
        ClassPathResource("examples/afile.json").file.readText()
    }
}